#!/bin/bash

dst='/srv/hackt-im-stratum0.org/htdocs/35c3'
filename='s0vouchers.png'

function gendot(){
 (echo "strict digraph S0SocialWeb{" ;
  echo "subgraph {";
  wget -q -O - https://pad.stratum0.org/p/35c3vouchers/export/txt |
    awk '/^\*[0-9]+\. Fertig/{f=1;next} /^\*[0-9]+/{f=0} !/^\s+1\. /{next} f{print tolower($0)}'|
    sed -E -e "s/^\s*[0-9]+.\s+([^, _]+).*an\s+([^, _]+).*,\s+([0-9]{2})\.([0-9]{2})\.[0-9]{2,4}\s*$/\"\\1\" -> \"\\2\" \nsubgraph cluster_\\3\\4 {margin=10;label=\"\\3.\\4.     \"; pencolor=transparent; labeljust=l; \"\\2\"}/" |
    sed -E -s "s/(.*)\"xxx(.*)xxx\"(.*)/\\1 \"\\2\" \\3 \n\"\\2\" [shape=underline]/";
  echo "}}")
}

case $1 in
    dot)
	gendot
	;;
    saveimg)
        gendot | dot -T png > $dst/$filename
        ;;
    show)
        gendot | dot -T gif | feh -
        ;;
    *)
        echo "Usage: $0 <saveimg|show>"
        echo "  saveimg - Save image"
        echo "  show - Shoe imahge"
        ;;
esac
exit 0
